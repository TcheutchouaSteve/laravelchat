<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

// Creating a grop of routes to apply to the filter in app/filter
Route::group(["before" => "auth"], function() {
    Route::any("/profile", [
        "as"
        => "user/profile",
        "uses" => "UserController@profile"
    ]);
});

Route::post("/login", [
    "as" => "login", "uses" => "SessionController@login"
]);

Route::get("/login", [
    "as" => "getLogin", "uses" => "SessionController@getLogin"
]);

Route::any("/request", [
    "as" => "user/request",  "uses" => "UserController@request"
]);

Route::any("/reset/{token}", [
    "as" => "user/reset", "uses" => "UserController@reset"
]);

Route::any("/logout", [
    "as"
    => "logout",
    "uses" => "SessionController@logout"
]);
Route::get("/signup", [
     "as" => "signup",
    "uses" => "SessionController@signup"
]);

Route::post("/signup", [
    "as" => "signup",
    "uses" => "SessionController@handleSignup"
]);

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'SessionController@activateAccount'
]);

Route::get('/home', [
    'as' => 'home',
    'uses' => 'SiteController@home'
]);

Route::get('/user/picture', [
    'as' => 'register_pic',
    'uses' => 'SiteController@addPhoto'
]);

Route::any('/user/setPhoto', [
    'as' => 'addAccountPic',
    'uses' => 'SiteController@postAddPhoto'
]);


/******** Routes related to the chats and the corresponding controller methods *********************/
/***************************************************************************************************/

Route::any('/chat/{action}',[
    'as' => 'chatActions',
    'uses' => 'ChatController@handleAjaxActions'
]);


/*Route::any('/chat/{action}',function($action){
    return array(
        'action' => $action
    );
});*/