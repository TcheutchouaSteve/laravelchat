<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');
        $this->call('UserTableSeeder');
        $this->command->info('User table seeded!');
	}
}


class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
            'name' => 'Tcheutchoua',
            'password' => Hash::make('okpass'),
            'email' => 'tcheutchouasteve@gmail.com',
            'picture' => '/img/gravatar.png',
            'gender' => 'male',
            'activation_state' => 'on',
        ));
        User::create(array(
                'name' => 'Rocard',
                'password' => Hash::make('rocard'),
                'email' => 'rocard@gmail.com',
                'picture' => '/img/gravatar.png',
                'gender' => 'male',
                'activation_state' => 'on',
            )
        );
    }
}
