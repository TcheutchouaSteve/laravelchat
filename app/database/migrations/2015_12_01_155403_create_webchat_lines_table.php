<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebchatLinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('webchat_lines', function(Blueprint $table)
        {
            $table->increments("id");
            $table->string('author')->index();
            $table->string('gravatar')->index();
            $table->string('text')->index();
            $table->timestamp('ts');
            // Adds created_at and updated_at columns , these attributes must be there to extend a model
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('webchat_lines');
	}

}
