<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration {

	/**f
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			Schema::create("users", function(Blueprint $table) {
			$table->increments("id");
			$table->string("name");
			$table->string("password");
			$table->string("email");
            $table->string('gender');
			$table->string("picture");
            $table->string('activation_key', 128);
            $table->enum('activation_state', array('on','off', 'deactivate'));
            $table->enum('online_status',array('online','offline'));
            $table->dateTime('last_login')->nullable();
			$table->string("remember_token")->nullable();
			$table->timestamps();
			});
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			Schema::dropIfExists("user");
		});
	}

}
