<!--
/**
 * Created by PhpStorm.
 * User: Tcheutchoua Steve
 * Date: 10/11/15
 * Time: 22:36
 */ -->
@extends("layouts/layout")
@section("content")
@if(Session::has('alertMessage'))
<div class="alert alert-dismissable alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>Session::get('alertMessage')</strong>
</div>
@endif

@if(Session::has('alertError'))
<div class="alert alert-dismissable alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>Session::get('alertError')</strong>
</div>
@endif
<div class="row">
    <div class="col-sm-5 col-sm-offset-4">

        <div class="panel panel-default">
            <div class="panel-heading"><h3>You're almost there!</h3></div>
            <div class="panel-body" id="completeswag">
                <small>Edit your profile photo if you'd like, then click "Complete Registration"</small>
                <div> <br />
                    <img src="URL::to('images/default.png')" class="img-rounded" id="imageswag" /><br /><br />
                    <small><a href="#" data-toggle="modal" data-target="#pictureModal">change photo</a></small> <br />
                    <a href="URL::Route('addAccountPic')" class="btn btn-primary btn-sm">Complete Registration</a><br />
                </div>
            </div>
        </div>
    </div>
</div>
@stop