<!--/**
 * Created by PhpStorm.
 * User: Tcheutchoua Steve
 * Date: 31/10/15
 * Time: 18:36
 */
-->
@extends("layouts/layout")
@section("content")
@if(Session::has('alertMessage'))
<div class="alert alert-dismissable alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('alertMessage')}}</strong>
</div>
@endif

@if(Session::has('alertError'))
<div class="alert alert-dismissable alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('alertError')}}</strong>
</div>
@endif
<div class="col-sm-3"></div>
<div class="col-sm-6">
    <h3 class="text-center">Edit Profile</h3>
    <form role="form" method="post" action="{{URL::Route('signup') }}">
        <div class="form-group">
            <label for="exampleInputEmail1">Full Name</label>
            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Full name" name="name" value="{{ Auth::user()->name}}">
            <span class="badge alert-danger">{{ ($errors->has('name') ? $errors->first('name') : '') }}</span>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
            <span class="badge alert-danger">{{ ($errors->has('password') ? $errors->first('password') : '') }}</span>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Confirm Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder=" Confirm Password" name="confirmed_password">
            <span class="badge alert-danger">{{ ($errors->has('comfirmed_password') ? $errors->first('comfirmed_password') : '') }}</span>
        </div>
        <br/>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">Update Account</button>
        </div>
    </form>
</div>
<div class="col-sm-3"></div>
@stop