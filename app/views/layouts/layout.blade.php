<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
<title > Tutorial</title >
</head >
<body >

@include ("layouts/header")
    <div class="content">
        <div class="container" >
            @yield("content")
        </div>
    </div>
    @include("layouts/footer")
</body>
</html>