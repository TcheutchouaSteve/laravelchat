
<!--/**
 * Created by PhpStorm.
 * User: Tcheutchoua Steve
 * Date: 10/29/15
 * Time: 9:36 PM
 */ -->
@section("header")
<nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid" style="float: right;" >
        @if (Auth::check())
        <a href="{{ URL::route('logout') }}" class="btn btn-success active navbar-left navbar-btn" role="button" >
        logout
        </a> |
        <a href="{{ URL::route('user/profile') }}" class="btn btn-success active navbar-left navbar-btn" role="button" >
        profile
        </a>
        @else
        <a href="{{ URL::route('getLogin') }}" class="btn btn-success active navbar-left navbar-btn" role="button" >
        login
        </a>
        @endif
    </div><!-- /.container-fluid -->
</nav>
@show