@extends("layouts/layout")
@section("content")
@if(Session::has('alertMessage'))
<div class="alert alert-dismissable alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('alertMessage')}}</strong>
</div>
@endif

@if(Session::has('alertError'))
<div class="alert alert-dismissable alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('alertError')}}</strong>
</div>
@endif
    <div col-sm-2>

    </div>
    <div col-sm-8>
    <form class="form-horizontal" role="form" method="post" action="{{URL::Route('login') }}">
        <div class="form-group" >
            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-8">
                <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email">
                <span class="badge alert-danger">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-8">
                <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password">
                <span class="badge alert-danger">{{ ($errors->has('password') ? $errors->first('password') : '') }}</span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> Remember me
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default" name="login">Login</button>
            </div>
        </div>
    </form>
    <p> Don't have an account ? </p>
    <a href="{{ URL::route('signup') }}" class="btn btn-success active navbar-left" role="button" > Signup </a>
    </div>
    <div class="col-sm-2">

    </div>
@stop
