<!--/**
 * Created by PhpStorm.
 * User: Tcheutchoua Steve
 * Date: 03/11/15
 * Time: 14:40
 */-->
@extends("layouts/layout")
@section("content")
@if(Session::has('alertMessage'))
<div class="alert alert-dismissable alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('alertMessage')}}</strong>
</div>
@endif

@if(Session::has('alertError'))
<div class="alert alert-dismissable alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{Session::get('alertError')}}</strong>
</div>
@endif
<div class="col-sm-3"></div>
<div class="col-sm-6">
    <h3 class="text-center">Signup</h3>
    <br/>
    <form role="form" method="post" action="{{URL::Route('signup') }}">
        <div class="form-group">
            <label for="exampleInputEmail1">Full Name</label>
            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Full name" name="name" value="{{ Input::old('name') != NULL ? Input::old('name') : '' }}">
            <span class="badge alert-danger">{{ ($errors->has('name') ? $errors->first('name') : '') }}</span>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="email" value="{{ Input::old('email') != NULL ? Input::old('email') : '' }}">
            <span class="badge alert-danger">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
            <span class="badge alert-danger">{{ ($errors->has('password') ? $errors->first('password') : '') }}</span>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Confirm Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder=" Confirm Password" name="confirmed_password">
            <span class="badge alert-danger">{{ ($errors->has('comfirmed_password') ? $errors->first('comfirmed_password') : '') }}</span>
        </div>
        <div class="form-group" >
            <label for="exampleInputPassword1" >Gender</label>

        <div class="radio">
            <label>
                <input type="radio" name="gender" id="optionsRadios2" value="male">
                Male
            </label>
            <label>
                <input type="radio" name="gender" id="optionsRadios2" value="female">
                Female
            </label>
            <span class="badge alert-danger">{{ ($errors->has('gender') ? $errors->first('gender') : '') }}</span>
        </div>
        <br/>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="terms"> Agree to <a href="#">terms and conditions</a>
                <span class="badge alert-danger">{{ $errors->first('terms') }}</span>
            </label>
        </div>
        <br/>
        <div class="text-center">
            <button type="submit" class="btn btn-primary">Create Account</button>
        </div>
    </form>
</div>
<div class="col-sm-3"></div>
@stop