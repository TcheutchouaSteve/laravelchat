<!--/**
 * Created by PhpStorm.
 * User: Tcheutchoua Steve
 * Date: 03/11/15
 * Time: 16:40
 */
 -->
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Activate Account</h2><br><br>

<div>
    Hi {{$name}},<br>
    Welcome to <a href="#">Mentor a Leader</a>, please follow the link below to activate your account.<br><br>
    To activate your account, follow link: {{ URL::to('register/verify/'.$activation_code)}}.<br/>
    If you did not register at <a href="#">Mentor a Leader</a>, please ignore this email.<br><br>
</div>
<h3>Mentor a leader team</h3>
</body>
</html>
