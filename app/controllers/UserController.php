<?php

/**
 * Created by PhpStorm.
 * User: Tcheutchoua Steve
 * Date: 10/29/15
 * Time: 10:59 PM
 */
class UserController extends Controller
{


    public function profile(){
        return View::make("user/profile");
    }

    public function request()
    {
        if($this->isPostRequest()){
            $response = $this->getPasswordReminderResponse();

            if($this->isInvalidUser($response)){
                return Redirect::back()
                    ->withInput()
                    ->with("error", Lang::get($response));
            }
            return Redirect::back()
                ->with("status", Lang::get($response));
             }
        return View::make("user/request");
    }

    protected function getPasswordRemindResponse()
    {
        return Password::remind(Input::only("email"));
    }

    protected function isInvalidUser($response)
    {
        return $response === Password::INVALID_USER;
    }

    public function reset($token)
    {
        if ($this->isPostRequest()) {
            $credentials = Input::only(
                    "email",
                    "password",
                    "password_confirmation"
                ) + compact("token");
            $response = $this->resetPassword($credentials);

            if ($response === Password::PASSWORD_RESET) {
                return Redirect::route("user/profile");
            }

            return Redirect::back()
                ->withInput()
                ->with("error", Lang::get($response));
        }
        return View::make("user/reset", compact("token"));
    }

    protected function resetPassword($credentials)
    {
        return Password::reset($credentials, function($user, $pass) {
            $user->password = Hash::make($pass);
            $user->save();
        });
    }


}