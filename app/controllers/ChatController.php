<?php
/**
 * Created by PhpStorm.
 * User: Tcheutchoua Steve
 * Date: 01/12/15
 * Time: 05:17
 */

class ChatController extends BaseController{

    var $lastID ;

    public function checkLogged(){
        $response = array('logged' => false);

        //Check if the user is logged in and return necessary data to jquery / ajax
        if(Auth::user()){
            $response['logged'] = true;
            $response['loggedAs'] = array(
                'name'		=> Auth::user()->name,
                'gravatar'	=> Auth::user()->picture
            );
        }
        //return Response::json(array('name' => 'Steve', 'state' => 'CA'));
        return $response;
    }

    public function submitChat($ChatText){
        if(!Auth::user()){

        }
        // make this route accessible only to authenticated users
        if(!$ChatText){
            throw new Exception('You haven\' entered a chat message.');
        }

        $chat = new ChatLine();
        $chat->author = Auth::user()->name ;
        $chat->gravatar = Auth::user()->picture ;
        $chat->text = $ChatText;
        $chat->save();

        // return the status as submited and the id of the last inserted chat for retrieval efficiency
        return array(
            'status'	=> 1,
            'insertID'	=> $chat->id
        );
    }

    public function getUsers(){
        $users = array();
        $users = DB::table('users')->where('online_status', '=', 'online')->get();
        $total = DB::table('users')->where('online_status', '=', 'online')->count();

        return array(
            'users' => $users,
            'total' => $total
        );
    }

    public function getChats($lastID){
        // id of previous last chat that was taken
        $lastID = (int)$lastID ;

        $result = DB::table('webchat_lines')->where('id', '>', $lastID)->orderBy('id', 'ASC')->get();
        $chats = array();

        //return (print_r($result));
        //while($chat = $result->fetch_object()){
        foreach($result as $chat){
            // Returning the GMT (UTC) time of the chat creation:

            $chat->time = array(
                'hours'		=> gmdate('H',strtotime($chat->ts)),
                'minutes'	=> gmdate('i',strtotime($chat->ts))
            );

            $chat->gravatar = Auth::user()->picture;

            $chats[] = $chat;
        }
        //return (print_r($chats));
        //echo (print_r($chats));
        return array('chats' => $chats);
    }

    public function handleAjaxActions($action){

        $response = array();
        try{
            switch($action){
            case 'checkLogged':
                $response = $this->checkLogged();
                break;

            case 'submitChat':
                $chatText = Input::get('chatText');
               $response = $this->submitChat($chatText);
                break;

            case 'getUsers':
                $response = $this->getUsers();
                break;

            case 'getChats':
                $id = Input::get('lastID');
                $response = $this->getChats($id);
                $response['id'] =  $id;
                break;

            default:
                throw new Exception('Wrong action');
        }

        //echo "This is a good day";
        return Response::json($response);
        }
        catch(Exception $e){
            die(json_encode(array('error' => $e->getMessage())));
        }
    }

}