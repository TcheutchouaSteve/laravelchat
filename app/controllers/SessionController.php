<?php
/**
 * Created by PhpStorm.
 * User: Tcheutchoua Steve
 * Date: 03/11/15
 * Time: 13:27
 */
class SessionController extends BaseController{



    public function getLogin(){
        return View::make("userSession.login");
    }


    public function login()
    {
        $login_data = Input::all();
        $login_rules = array(
            'email'	  				=>'required|email',
            'password'				=>'required',
        );

        if ($this->isPostRequest()) {
            $loginValidator = Validator::make($login_data,$login_rules);
            if ($loginValidator->passes()) {
               if(Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))){

                    //  check if account is active
                    if(Auth::user()->activation_state == "off")
                    {
                        Auth::logout();
                        return Redirect::back()->with('alertError', "please check your mail to activate account.");
                    }

                    // If the user's account in question has been locked
                    if(Auth::user()->activation_state == "deactivate")
                    {
                        Auth::logout();
                        return Redirect::back()->with('alertError', "contact support@mentoraleader.com, your account has been blocked.");
                    }

                    Auth::user()->last_login = new DateTime();
                    Auth::user()->online_status="online";
                    Auth::user()->save();
                    //Checking if the profile picture is set before redirecting him to the home
/*                   if(Auth::user()->picture == null){
                        return Redirect::route('register_pic');
                   }else
                    return Redirect::to('home');*/
                   return View::make('index');
                }
                //echo "Validation passed";
            }
            else {
                //return Redirect::back()->withInput()->withErrors($loginValidator);
                return Redirect::back()->with('alertError', "invalid account details.");
                //echo " validation failed ";
            }
        }
    }


    protected function isPostRequest()
    {
        return Input::server("REQUEST_METHOD") == "POST";
    }

    public function logout()
    {
        Auth::user()->online_status = "offline";
        Auth::user()->save();
        Auth::logout();
        return Redirect::route("login");
    }

    public function  signup(){
        return View::make("userSession.signup");
    }

    public function handleSignup(){
        $registerData = Input::all();
        $registerRules = array(
            'name'					=>"required|min:4",
            'email'	  				=>'required|email|unique:users',
            'password'				=>'required|alpha_num|min:6',
            'confirmed_password'    =>'required|alpha_num|same:password',
            'gender'                =>'required',
            'terms'	   				=>'required',
        );
        $registerValidator = Validator::make($registerData,$registerRules);
        if( $registerValidator->passes()) {
            $user = new User();
            $user->name = Input::get('name');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->gender = Input::get('gender');
            $user->activation_key = bin2hex(openssl_random_pseudo_bytes(16));
            $user->activation_state = "off";
            $user->save();
            // send activation link
            $mailData = array(
                'name'=> Input::get('name'),
                'activation_code'=> $user->activation_key
            );
            Mail::send('emails.activate',$mailData,
                function($message) {
                    $message->subject("Mentor A Leader  account activation");
                    $message->to(Input::get('email'));
                }
            );
            return Redirect::to('login')->with('alertMessage',"check email, to activate account.");
        }
        if($registerValidator->fails()) {
            //echo "account create validation failed";
            return Redirect::back()->withInput()->withErrors($registerValidator);
        }
    }

    public function activateAccount($activation_code){
        $activationCheck = User::where('activation_key','=',$activation_code)->count();
        if($activationCheck == 1) {
            $activateAccount = User::where('activation_key','=',$activation_code)->first();
            $activateAccount->activation_state = "on";
            $activateAccount->activation_key = NULL;
            $activateAccount->save();
            return Redirect::to('login')->with('alertMessage',"account activated, you can now login.");
        }
        else {
            return Redirect::to('/');
        }
    }
}